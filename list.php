<?php
	include 'php/database.php';
	include 'php/tablesData.php';
	include 'php/usb.php';
	include 'php/notebook.php';
	include 'php/furniture.php';
	$db = new Database;
	$deleteObj = new Data;
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="css/list_styles_main.css">
	<link href="https://fonts.googleapis.com/css?family=Quicksand&display=swap" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<title>Document</title>
</head>
<body>
	<!-- Main Container-->
	<div class="main-container">
		<!-- Header Section -->
		<div class="header-section">
		<h1>Product Add</h1>
			<form action="list.php" method="POST">
				<button type="submit" name="submit" class="submitButton">Delete Selections</button>

				<select name="" id="deleteAction">
					<option value="massdelete">Mass Delete Action</option>
				</select>
		</div> <!--  End header section-->

			<!-- Body section -->
			<div class="body-section">
				<?php
					// I moved rendering script in here rather than using in php file...
					//$data = new Data();
					//$notebook = new Notebook();
					//$furniture = new Furniture();

					$sql = "select * from products";
					$class = "card";
					$type = "checkbox";
					$name = "deleteCheckBoxes[]";
					$id = "checkBox";
					$result = $db->connect()->query($sql);
					if ($result) {
						while ($data = $result->fetch_object()) { // Fetching the products object
							$objectData = $data;
							echo "<div class=".$class.">";
							echo "<input type=".$type." name=".$name." value=".$objectData->id." id=".$id.">";
							echo "<h3>".$objectData->sku."</h3>";
							echo "<h3>".$objectData->name."</h3>";
							echo "<h3>".$objectData->price."$</h3>";
							if (!$objectData->size == 0) echo	"<h3>".$objectData->size."GB</h3>";
							if (!$objectData->weight == 0) echo	"<h3>".$objectData->weight."KG</h3>";
							if (!$objectData->height == 0 and !$objectData->width == 0 and !$objectData->length == 0) {
									echo "<h3>Dimensions: ".$objectData->height."X".$objectData->width."X".$objectData->length."</h3>";
							}
							echo "</div>";
						}
					}
					else {
						return "error getting data object";
					}

					// Delete checked product
					$deleteObj->deleteCheckBoxes();
				?>
			</form>
		</div> <!-- Body section -->
	</div> <!-- End Main Container-->

	<script src="js/script.js"></script>
</body>
</html>
