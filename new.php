<?php
	include 'php/database.php';
	include 'php/TablesData.php';
	include 'php/products.php';
	include 'php/usb.php';
	include 'php/notebook.php';
	include 'php/furniture.php';

	$data = new Database();
	$usb = new Usb();
	$notebook = new Notebook();
	$furniture = new Furniture();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="css/add_styles.css">
	<link href="https://fonts.googleapis.com/css?family=Quicksand&display=swap" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<title>Document</title>
</head>
<body>
	<!-- Main Container-->
	<div class="main-container">

		<!-- Header Section -->
		<div class="header-section">
			<form action="new.php" method="POST">
				<h1>Product Add</h1>
				<button type="submit" name="submit" class="submitButton">Save</button>
		</div> <!-- End Header Section -->

		<!-- Body Section -->
		<div class="body-section">

				<h2>SKU</h2><input type="text" id="input1" name="sku"><br>
				<h2>Name</h2><input type="text" id="input2" name="name"><br>
				<h2>Price</h2><input type="text" id="input3" name="price"><br>
				<h2>Type Switcher</h2>
				<select id="input4">
					<option value="usb">USB FLASH</option>
					<option value="notebook">Notebook</option>
					<option value="table">Furniture</option>
				</select>

				<div id="usb" class="form-div">
					<h3>Size</h3><input type="text" name="size"><br>
					<p>* Please provide size in GB's</p>
				</div>

				<div id="notebook" class="form-div">
					<h3>Weight</h3><input type="text" name="weight">
					<p>*Please provide weight in KG's</p>
				</div>

				<div id="table" class="form-div">
					<h3>Height</h3><input type="text" name="height"><br>
					<h3>Width</h3><input type="text" name="width"><br>
					<h3>Length</h3><input type="text" name="length"><br>
					<p>*Please provide dimensions in Meters</p>
				</div>
			</form>
		</div> <!-- End Body Section-->
	</div> <!-- End Main Container-->
	<?php
		if (isset($_POST['submit'])) {
			/*
				Determining to witch type of product
				class insertIntoTable() method
				should be called for inserting data
				in to the Database using fields
			*/
			$Name = $usb->getName();
			$SKU = $usb->getSKU();
			$Price = $usb->getPrice();

			// Form Validation
			if($Name == '') echo "<script>alert('Please fill name field')</script>";
			if($SKU == '') echo "<script>alert('Please fill sku field')</script>";
			if($Price == '') echo "<script>alert('Please fill price field')</script>";

			// Inserting data using fields values
			if(!$usb->getSize() == '') {
				$usb->insertIntoTable("products");
			}

			if(!$notebook->getWeight() == '') {
				$notebook->insertIntoTable("products");
			}

			if(!$furniture->getHeight() == '' and !$furniture->getWidth() == '' and !$furniture->getLength() == '') {
				$furniture->insertIntoTable("products");
			}
		}
	?>
	<script src="js/script.js"></script>
</body>
</html>
