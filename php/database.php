<?php

// This is my first try using OOP php. For that as a most part I used mmtuts youtube
// channel as a helpful source to develop my logic. Sorry for that.

class Database {
	private $server;
	private $user;
	private $password;
	private $dbname;

	public $con;
  public $error;

	public function __construct() {
     $this->con = mysqli_connect("localhost", "root", "", "products");
     if(!$this->con) {
          echo 'Database Connection Error ' . mysqli_connect_error($this->con);
     }
	}

	/* Using this method to get All products
		 data in to the TablesData class*/
	public function connect() {
		$this->server = "localhost";
		$this->user = "root";
		$this->password = "";
		$this->dbname = "products";

		$conn = new mysqli($this->server, $this->user, $this->password, $this->dbname);
		return $conn;
	}
}
