<?php

/* This class extends Products class and implements
  functions with data insertion fucntion + own method
*/
class Furniture extends Products {
  public $height;
  public $width;
  public $length;

  public function __constr($sku, $name, $price, $height, $width, $length) {
      $this->$sku = $_POST['sku'];
      $this->$name = $_POST['name'];
      $this->$price = $_POST['price'];
      $this->$height= $_POST['height'];
      $this->$width = $_POST['width'];
      $this->$length = $_POST['length'];
  }

  // Getters
  function getHeight() {
    if (isset($_POST['submit'])) {
      $height = $_POST['height'];
    }
    return $height;
  }

  function getWidth() {
    if (isset($_POST['submit'])) {
      $width = $_POST['width'];
    }
    return $width;
  }

  function getLength() {
    if (isset($_POST['submit'])) {
      $length = $_POST['length'];
    }
    return $length;
  }

  // Setters
  public function setHeight($height) {
    $this->height = $height;
  }

  public function setWidth($width) {
    $this->width = $width;
  }

  public function setLength($length) {
    $this->length = $length;
  }
  // Getting all data to insert into DB
  public function insertIntoTable($table_name) {
    $sku = $this->getSKU();
    $name = $this->getName();
    $price = $this->getPrice();
    $height = $this->getHeight();
    $width = $this->getWidth();
    $length = $this->getLength();

    $string = "INSERT INTO ".$table_name." (sku,name,price,size,weight,height,width,length) VALUES ('$sku', '$name', '$price', '','','$height','$width','$length');" ;
    if(mysqli_query($this->con, $string)) {
      echo "<script>alert('Data is inserted!')</script>";
    }
    else {
      echo mysqli_error($this->con);
    }
  }
}
