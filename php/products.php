<?php

// Abstract class which holds general logic for products
abstract class Products extends Data {

  // Getters which are common to all derived classes(DRY principle)
  function getSKU() {
    if (isset($_POST['submit'])) {
      $sku = $_POST['sku'];
    }
    return $sku;
  }

  function getName() {
    if (isset($_POST['submit'])) {
      $name = $_POST['name'];
    }
    return $name;
  }

  function getPrice() {
    if (isset($_POST['submit'])) {
      $price = $_POST['price'];
    }
    return $price;
  }

  // Functions which should be extended in the type specific class
  // Here I wanted to use abstract keyword to implement specific method in the specific
  // class but I think thats wrong in my example(I have to implement all methods in all derived classes if I use abstract keyword for them).
  // this is my approach for this...
  public function getSize() { } // Extended in USB class
  public function getWeight() { } // Extended in Notebook class
  public function getHeight() { } // Extended in Furniture class
  public function getWidth() { } // Extended in Furniture class
  public function getLength() { } // Extended in furniture class
  //abstract function insertIntoTable($table_name);
}
